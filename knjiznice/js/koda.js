
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 function klicGenerirajPodatke(){
	$('#geneeeriraj').text("");
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
}

function generirajPodatke(stPacienta) {
  ehrId = "";
	
	if(stPacienta == 1){
		ehrId = "7dad6fbf-8ae6-4414-aca8-0f81143e44be"
		var tezz = Math.round((Math.random() * 100) /2);
		while(tezz>5){
			tezz=tezz/2;
		}
		var viss = Math.round((Math.random() * 100)/2+100/6);
		while(viss>70){
			viss=viss/1.5;
		}
		var siss = Math.round((Math.random() * 100) /2+60);
		var diss = Math.round((Math.random() * 100) /3+40);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#geneeeriraj').append("</br>Peter Pan: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}
	else if(stPacienta == 2){
		ehrId = "ef8e6d49-af85-49c6-ad49-2dd9a27161ba"
		var tezz = Math.round((Math.random() * 100) /2+80);
		while(tezz>4){
			tezz=tezz/2.2;
		}
		var viss = Math.round((Math.random() * 100)/2+165);
		while(viss>50){
			viss=viss/1.1;
		}
		var siss = Math.round((Math.random() * 100) /2+110);
		var diss = Math.round((Math.random() * 100) /3+70);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#geneeeriraj').append("</br>luka KLjuka: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}
	else if(stPacienta == 3){
		ehrId = "db3d3030-0a7f-4ad0-be50-71b9fcb19212"
		var tezz = Math.round((Math.random() * 100) /2+30);
		while(tezz>2.7){
			tezz=tezz/1.5;
		}
		var viss = Math.round((Math.random() * 100)/2+120);
		while(viss>90){
			viss=viss/2;
		}
		var siss = Math.round((Math.random() * 100) /2+80);
		var diss = Math.round((Math.random() * 100) /3+65);
		dodajPodatkeEHR(ehrId,viss,tezz,siss,diss);
		$('#geneeeriraj').append("</br>Princeska Hana: Višina: "+viss+"cm, Teža: "+tezz+"kg, Tlak: "+siss+"/"+diss+"mmHg");
	}

  return ehrId;
}

function dodejPodatke(){
	var vis = $('#visina').val();
	var tez = $('#teza').val();
	var sis = $('#sistol').val();
	var dis = $('#diastol').val();
	var idd = $('#EHRID').val();
	dodajPodatkeEHR(idd,vis,tez,sis,dis);
}

function dodajPodatkeEHR(ehrId, visina, teza, sistol, distol){
	var sessid = getSessionId();
	$.ajaxSetup({
	headers: {
		"Ehr-Session":sessid
	}});
	var data = {
		"ctx/language": "en",
		"ctx/territory": "SI",
		"ctx/time": "2016-01-01",
		"vital_signs/height_length/any_event/body_height_length": visina,
        	"vital_signs/body_weight/any_event/body_weight": teza,
        	"vital_signs/blood_pressure/any_event/systolic": sistol,
        	"vital_signs/blood_pressure/any_event/diastolic": distol
	};
	var parampampam = {
		ehrId: ehrId,
		templateId: 'Vital Signs',
		format: 'FLAT',
		committer: 'jst'
	};
	$.ajax({
		url: baseUrl + "/composition?" + $.param(parampampam),
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(data),
		success: function(odgovor){
			
	$('#geneeeriraj2').text("Podatki dodani");
		},
		error: function(error){
	$('#geneeeriraj').text("napaka");
			console.log("napaka"+error);
		}
	});
}

function dodajanje_EHR(){
	dodejVBazo($('#dodaj_Ime').val(),$('#dodaj_Priimek').val(),$('#dodaj_Datum').val());
}

function dodejVBazo(iime, ppriimek, ddatum){
	sessId = getSessionId();
	$.ajaxSetup({
	headers: {
		"Ehr-Session": sessId
	}});
	var odg = $.ajax({
		url: baseUrl + '/ehr',
		async: false,
		type: 'POST',
		success: function(data) {
			var ehrId = data.ehrId;
			var partyData = {
				firstNames: iime,
				lastNames: ppriimek,
				dateOfBirth: ddatum,
				partyAdditionalInfo: [{
					key: "ehrId", value: ehrId
				}]
		};
		$.ajax({
			url: baseUrl + "/demographics/party",
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(partyData),
			success: function(party){
				$('#obvestilaDudej').text('Vaš EHR ID: ' + ehrId);
			},
			error: function(error){
				$('#obvestilaDudej').text('napaka');
			}
		});
	}
	});
}

function rezultati(){
sessionId = getSessionId();
	var	iddd = $('#EHRIDDD').val();
	console.log(iddd);
	$('#Pobrisi').text("");
	if (!iddd || iddd.trim().length == 0) {
		$('#Pobrisi').text("Prišlo je do napake. Preverite če ste pravilno unesli ID. ");
		return;
	}
	$.ajax({
            url: baseUrl + "/view/" + iddd + "/" + "blood_pressure?" + $.param({
                limit: 25
            }),
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
		success: function(tlk){
	$.ajax({
                    url: baseUrl + "/view/" + iddd + "/" + "weight",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },
		success: function(tez){
	$.ajax({
                    url: baseUrl + "/view/" + iddd + "/" + "height",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },	
		success: function(vis){
			prikaziPodatke(tlk,tez,vis);
		},
	error: function(){
		console.log("napaka");
	}
});
	},
	error: function(){
		console.log("napaka");
	}
});
	},
	error: function(){
		console.log("napaka");
	}
});
	
}

function prikaziPodatke(tlk,tez,vis) {
	var i = vis.length-1;
	var teza = tez[i].weight;
	var tmp = 0;
	var visina = vis[i].height;
	var sistolicni = tlk[i].systolic;
	var diastolicni = tlk[i].diastolic;
	var tezamesage = "";
	var visinamessage = "";
	var sistolmessage = "";
	var diastolmessage = "";
	
	console.log(teza,visina,sistolicni,diastolicni);
	
	if(teza < 2.7){
		tmp++;
		$('#Pobrisi').append("<p>Teža dojenčka je PODPOVPREČNA</p>");
		tezamessage = "Na%20svet%20je%20priskakljal%20novorojenček%20,ki%20ima%20PODPOVPREČNO%20težo,%20";
	}
	if(teza > 3.3){
		tmp++;
		$('#Pobrisi').append("<p>Teža dojenčka  je NADPOVPREČNA</p>");
		tezamessage =  "Na%20svet%20je%20priskakljal%20novorojenček%20,ki%20ima%20NADPOVPREČNO%20težo,%20";
	}
	if(teza <= 3.3 && teza >= 2.7 ){
		$('#Pobrisi').append("<p>Teža dojenčka  je POVPREČNA</p>");
		tezamessage =  "Na%20svet%20je%20priskakljal%20novorojenček%20,ki%20ima%20POVPREČNO%20težo,%20";
	}
	if(visina < 50){
		tmp++;
		$('#Pobrisi').append("<p>Višina dojenčka je PODPOVPREČNA</p>");
		visinamessage = "PODPOVPREČNO%20višino,%20";
	}
	if(visina > 55){
		tmp++;
		$('#Pobrisi').append("<p>Višina dojenčka je NADPOVPREČNA</p>");
		visinamessage = "NADPOVPREČNO%20višino,%20";
	}
	if(visina <= 55 && visina >= 50){
		$('#Pobrisi').append("<p>Višina dojenčka je POVPREČNA</p>");
		visinamessage = "POVPREČNO%20višino,%20";
	}
	if(sistolicni < 117){
		tmp++;
		$('#Pobrisi').append("<p>Sistolični tlak dojenčka je PODPOVPREČEN</p>");
		sistolmessage = "PODPOVPREČEN%20sistolični%20tlak%20in%20";
	}
	if(sistolicni > 123){
		tmp++;
		$('#Pobrisi').append("<p>Sistolični tlak dojenčka je NADPOVPREČEN</p>");
		sistolmessage = "NADPOVPREČEN%20sistolični%20tlak%20in%20";
	}
	if(sistolicni >= 117 && sistolicni <= 123 ){
		$('#Pobrisi').append("<p>Sistolični tlak dojenčka je POVPREČEN</p>");
		sistolmessage = "POVPREČEN%20sistolični%20tlak%20in%20";
	}
	
	if(diastolicni < 75){
		tmp++;
		$('#Pobrisi').append("<p>Diastolični tlak dojenčka je PODPOVPREČEN</p>");
		diastolmessage = "PODPOVPREČEN%20diastolični%20tlak.%20";
	}
	if(diastolicni > 85){
		tmp++;
		$('#Pobrisi').append("<p>Diastolični tlak dojenčka je NADPOVPREČEN</p>");
		diastolmessage = "NADPOVPREČEN%20diastolični%20tlak.%20";
	}

	if(diastolicni <= 85 && diastolicni >= 75){
		$('#Pobrisi').append("<p>Diastolični tlak dojenčka je POVPREČEN</p>");
		diastolmessage = "POVPREČEN%20diastolični%20tlak.%20";
	}
	//narisiGraf(tmp);
    narisiGraf2(teza);
    //console.log(beri_datoteko('dekleta_teza.txt'));
	narisiGraf3(visina);
	narisiGraf4(sistolicni);
	narisiGraf5(diastolicni);
	
	$('#SHARE').append("<p></p><h3 style='text-align:center;'>DELI Z OSTALIMI:</h3><p style='text-align:center;'><a class='twitter-share-button' href='https://twitter.com/intent/tweet?text="+tezamessage+visinamessage+sistolmessage+diastolmessage+"%20' data-size='large'><img src='https://static.addtoany.com/images/blog/tweet-button-2015.png' style='width:100px'></a></p>");
}

function narisiGraf2(num){
$(function() {
	var data = [num];
        $('#tolePaDodej2').highcharts({
		chart: {
			type: 'bar'
		},
            title: {
                text: 'TEŽA',
                x: -20 //center
            },
            xAxis: {
                categories: ['Teža dojenčka','Povprečje'],
            },
            yAxis: {
		max: 5,
		min: 0,
                title: {
       
                    text: 'kg'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080',
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
            name: 'Teža dojenčka',
            data: [data],
            }, {
            name: 'Povprečje',
            data: [3],
            }
            ]
        });
    });
}
function narisiGraf3(num){
$(function() {
	var data = [num];
        $('#tolePaDodej3').highcharts({
		chart: {
			type: 'bar'
		},
            title: {
                text: 'VIŠINA',
                x: -20 //center
            },
            xAxis: {
                categories: ['Višina dojenčka','Povprečje'],
            },
            yAxis: {
		max: 66,
		min: 0,
                title: {
                    text: 'cm'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080',
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
            name: 'Višina dojenčka',
            data: [data],
            }, {
            name: 'Povprečje',
            data: [52],
            }
         
            ]
        });
    });
}
function narisiGraf4(num){
$(function() {
	
	var data = [num];
        $('#tolePaDodej4').highcharts({
		chart: {
			type: 'bar'
		},
            title: {
                text: 'Sistolični tlak',
                x: -20 //center
            },
            xAxis: {
                categories: ['Sistolični tlak','Povprečje'],
            },
            yAxis: {
		max: 120,
		min: 0,
                title: {
                    text: 'mmHg'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080',
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
            name: 'Sistolični tlak',
            data: [data],
            }, {
            name: 'Povprečje',
            data: [120],
            }
         
            ]
        });
    });
}
function narisiGraf5(num){
$(function() {
	var data = [num];
        $('#tolePaDodej5').highcharts({
		chart: {
			type: 'bar'
		},
            title: {
                text: 'Diastolični tlak',
                x: -20 //center
            },
            xAxis: {
                categories: ['Diastolični tlak','Povprečje'],
            },
            yAxis: {
		max: 150,
		min: 0,
                title: {
                    text: 'mmHg'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080',
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
            name: 'Diastolični tlak',
            data: [data],
            }, {
            name: 'Povprečje',
            data: [80],
            }
         
            ]
        });
    });
}
var txt;
function preload(){
	txt=loadStrings("dekleta_teza.txt");
}




function beri_datoteko(filename) {
  var vrstice = [];
  $.get({
    url: filename,
    async: false,
    success: function(vsebina) {
      vrstice = vsebina.split('\n');
      for(var i = 0; i<vrstice.length; i++) {
        var vrstica = vrstice[i].split('\t');
        vrstice[i] = vrstica[5];
      }
    }
  });
  console.log(txt);
   console.log(vrstice);
  return vrstice;
}


$(window).load(function() {
    $("#poljubnipacienti").change(function() {
        $('#EHRIDDD').val($('#poljubnipacienti').val());
  

  
    });

});
